import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Main from '../views/Main.vue';
import News from '../views/News.vue';
import Favorites from '../views/Favorites.vue';
import MoviePage from '../views/MoviePage.vue';

import Vuex from '../store';

Vue.use(VueRouter);


const routes = [
  { path: '/', redirect: '/home' },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    beforeEnter: (to: any, from: any, next: any) => {
      if (Vuex.state.sessionId) {
        next();
      } else {
        next({ name: 'Login' });
      }
    },
    children: [
      {
        path: '',
        name: 'Main',
        component: Main,

      },
      {
        path: 'movie/:id',
        name: 'Movie',
        component: MoviePage,

      },
      {
        path: 'favorites',
        name: 'Favorites',
        component: Favorites,
      },
      {
        path: 'watch',
        name: 'News',
        component: News,
      },
    ],
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
  },
];

const router = new VueRouter({ routes, mode: 'history' });

export default router;
