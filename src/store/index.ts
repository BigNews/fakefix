import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import {
  addToWatchList, fetchFavorites, fetchWatchList, getTrendingMovies, markAsFavorite,
} from '@/services/api';
import { SnackbarProgrammatic as Snackbar, ToastProgrammatic as Toast } from 'buefy';


Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    searchValue: '',
    accountId: '',
    sessionId: '',
    requestToken: '',
    favorites: {},
    watchList: {},
    trending: { fetchDate: null, data: [] },
  },
  getters: {
    getInWatchListById: (state: any) => (id: string) => {
      if (state.watchList.results) {
        return state.watchList.results.find((watchListItem: any) => watchListItem.id === id);
      }
      return null;
    },
    getInFavoriteListById: (state: any) => (id: string) => {
      if (state.favorites.results) {
        return state.favorites.results.find((favoriteItem: any) => favoriteItem.id === id);
      }
      return null;
    },

  },
  mutations: {
    UPDATE_SEARCH_VALUE: (state: any, payload) => {
      state.searchValue = payload;
    },
    UPDATE_VALUE: (state: any, payload) => {
      state[payload.name] = payload.value;
    },
    SET_FAVORITES: (state: any, payload: any) => {
      state.favorites = payload;
    },
    SET_WATCH_LIST: (state: any, payload: any) => {
      state.watchList = payload;
    },
    LOGOUT: (state: any, payload: any) => {
      state.accountId = '';
      state.requestToken = '';
      state.sessionId = '';
      state.favorites = {};
      state.watchList = {};
    },
    SET_TRENDING_LIST: (state: any, payload: any) => {
      state.trending = payload;
    },
  },
  actions: {
    async addToFavorite({ commit, state, dispatch }: any, media) {
      try {
        await markAsFavorite(state.sessionId, state.accountId, media);
        Toast.open({
          message: media.favorite ? ' Braddock ! Je vous préviens, faites attention où vous mettez les pieds !' : 'Je l\'ai descendu le canasson !',
          type: 'is-success',
          position: 'is-top',
        });
        dispatch('fetchFavorites');
      } catch (e) {
        // console.log(e);
      }
    },
    async fetchFavorites({ commit, state }: any) {
      try {
        const results: any = await fetchFavorites(state.sessionId, state.accountId);
        commit('SET_FAVORITES', results);
      } catch (e) {
        // console.log(e);
      }
    },
    async addToWatchList({ commit, state, dispatch }: any, media) {
      try {
        await addToWatchList(state.sessionId, state.accountId, media);
        Toast.open({
          message: media.watchlist ? 'Un toit ouvrant sur un sous-marin, gros taré !' : 'Je l\'ai descendu le canasson !',
          type: 'is-success',
          position: 'is-top',
        });
        dispatch('fetchWatchList');
      } catch (e) {
        // console.log(e);
      }
    },
    async fetchWatchList({ commit, state }: any) {
      try {
        const results: any = await fetchWatchList(state.sessionId, state.accountId);
        commit('SET_WATCH_LIST', results);
      } catch (e) {
        // console.log(e);
      }
    },
    async fetchTrendingList({ commit, state }: any) {
      const today = new Date().toISOString().split('T')[0];

      if (!today || !state.trending.data.length || today !== state.trending.fetchDate) {
        const { results }: any = await getTrendingMovies();
        commit('SET_TRENDING_LIST', { data: results, fetchDate: today });
      }
    },
  },
  modules: {},
  plugins: [new VuexPersistence().plugin],
});
