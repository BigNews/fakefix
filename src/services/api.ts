import ky from 'ky';


export interface Movie {
  id: number;
  title: string;
  poster_path: string;
  overview: string;
  release_date: string;
}

export interface MoviesList {
  page: number;
  total_results: number;
  total_pages: number;
  result: Movie[];
}


const apiKey = process.env.VUE_APP_API_KEY;


const api = ky.create({
  prefixUrl: process.env.VUE_APP_API_URL,
  hooks: {
    beforeRequest: [
      (request) => {
        request.headers.set('Authorization', `Bearer : ${apiKey}`);
      },
    ],
  },
});

export function createRequestToken() {
  return api.get(`authentication/token/new?api_key=${apiKey}`).json();
}


export function logUser(userName: string, password: string, token: string) {
  // eslint-disable-next-line @typescript-eslint/camelcase
  return api.post(`authentication/token/validate_with_login?api_key=${apiKey}`, { json: { username: userName, password, request_token: token } });
}


export function searchMovie(searchValue: string) {
  return api.get(`search/movie?query=${searchValue}&api_key=${apiKey}`).json();
}

export function getMovie(id: string) {
  return api.get(`movie/${id}?api_key=${apiKey}`).json();
}

export function getVideo(id: string) {
  return api.get(`movie/${id}/videos?api_key=${apiKey}`).json();
}

export function getSessionId(token: string) {
  // eslint-disable-next-line @typescript-eslint/camelcase
  return api.post(`authentication/session/new?api_key=${apiKey}`, { json: { request_token: token } }).json();
}

export function getAccount(sessionId: string) {
  return api.get(`account?api_key=${apiKey}&session_id=${sessionId}`).json();
}

export function markAsFavorite(sessionId: string, accountId: string, media: {media_type: 'tv' | 'movie'; media_id: string; favorite: boolean}) {
  return api.post(`account/${accountId}/favorite?api_key=${apiKey}&session_id=${sessionId}`, { json: media });
}

export function addToWatchList(sessionId: string, accountId: string, media: {media_type: 'tv' | 'movie'; media_id: string; watchlist: boolean}) {
  return api.post(`account/${accountId}/watchlist?api_key=${apiKey}&session_id=${sessionId}`, { json: media });
}

export function getTrendingMovies() {
  return api.get(`trending/movie/day?api_key=${apiKey}`).json();
}

export function fetchFavorites(sessionId: string, accountId: string) {
  return api.get(`account/${accountId}/favorite/movies?api_key=${apiKey}&session_id=${sessionId}`).json();
}

export function fetchWatchList(sessionId: string, accountId: string) {
  return api.get(`account/${accountId}/watchlist/movies?api_key=${apiKey}&session_id=${sessionId}`).json();
}
